import React from 'react';

const dropDown = (props) => (
    <ul className="dropdown-menu">
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something else here</a></li>
        <li role="separator" className="divider"></li>
        <li><a href="#">Separated link</a></li>
  </ul>
)

export default dropDown;