import React, {Component} from 'react';
import NavbarHeader from './NavbarHeader';
import NavbaRight from './NavbaRight';
import Aux from '../../hoc/Auxx';
import './Header.css';
// Start Header
class Header extends Component{
    render(){
        return(
        <nav className="navbar navbar-inverse">
            <div className="container-fluid">
                <NavbarHeader />
                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <NavbaRight />
                </div>
            </div> 
        </nav>
        )
    }
}
export default Header;