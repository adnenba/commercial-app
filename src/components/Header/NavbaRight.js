import React from 'react';
import DropDown from './DropDown';

const navbaRight = (props) => (
<ul className="nav navbar-nav navbar-right">
    <li><a href="#">Link</a></li>
    <li className="dropdown">
        <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span className="caret"></span></a>
        <DropDown />
    </li>
</ul>
);

export default navbaRight;