import React from 'react';
import Logo from './Logo';
const navbarHeader = (props) => {
    const renderSpan = () =>{
       const buttons = [];
       for(let i = 0; i < props.numbers; i++){
           buttons.push(<span className="icon-bar" key={i} ></span>)
       }
       return(
           buttons
       )
    }
    return (
    <div className="navbar-header">
      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span className="sr-only">Toggle navigation</span>
                {renderSpan()}
      </button>
      <div className="col-md-2">
      <Logo />
      </div>
    </div>
)}
navbarHeader.defaultProps = {
    numbers: 3
}

export default navbarHeader;