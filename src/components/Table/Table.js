import React, { Component } from 'react';

class Table extends Component {

  state = {
    entites: [],
    specs: {},
    desc: false,
  }

  componentWillMount() {
    let specs = {};
    Object.keys(this.props.entites[0]).forEach(champ => {
      specs = { ...specs, [champ]: '' };
    });
    this.setState({ entites: this.props.entites, specs });
  }

  srt(desc, key) {
    return function (a, b) {
      return desc ? ~~(key ? a[key] < b[key] : a < b)
        : ~~(key ? a[key] > b[key] : a > b);
    };
  }

  onChangeSpec = (event, champ) => {
    const spec = event.target.value;
    const entites = this.props.entites;
    const specs = this.state.specs;
    specs[champ] = spec;
    let array = entites;
    const specsArray = Object.keys(specs);
    for (let i = 0; i < specsArray.length; i++)
      array = array.filter(item => {
        return (
          item[specsArray[i]].toString().toLowerCase().indexOf(specs[specsArray[i]].trim().toLowerCase()) !== -1
        );
      });
    this.setState({ entites: array });
  };

  sortColumn = (field) => {
    const state = this.state.entites;
    const desc = !this.state.desc;
    state.sort(this.srt(desc, field));
    this.setState({ entites: state, desc: desc });
  }

  displayInput = () => {
    return Object.keys(this.props.entites[0]).map((champ, i) => {
      return <th key={i} style={{ textAlign: 'center' }}><input type="text" size="15" onChange={(event) => this.onChangeSpec(event, champ)}
        style={{ fontWeight: 'normal' }} /></th>;
    });
  }

  capitalize = string => {
    return typeof string !== "string" ? string : string.charAt(0).toUpperCase() + string.slice(1);
  }

  displayTitle = () => {
    return Object.keys(this.props.entites[0]).map((champ, i) => {
      const arrow = this.state.desc ? 'glyphicon glyphicon-menu-down' : 'glyphicon glyphicon-menu-up';
      return (
        <th key={i} onClick={() => this.sortColumn(champ)} style={{ cursor: 'pointer' }}>{this.capitalize(champ)}
          <span className={arrow} style={{ marginLeft: '50%' }}></span></th>
      );
    });
  }

  displayField = () => {
    return (this.state.entites.map((entite, i1) => {
      return (
        <tr key={i1}>
          {Object.keys(entite).map((champ, i2) => {
            return <td key={i2}>{entite[champ]}</td>
          })}
        </tr>
      );
    })
    )
  }

  render() {
    return (
      <div className="table-responsive">
        <table className="table table-bordered table-hover table-striped">
          <thead >
            <tr className="warning">
              {this.displayInput()}
            </tr>
            <tr className="info">
              {this.displayTitle()}
            </tr>
          </thead>
          <tbody>
            {this.displayField()}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;