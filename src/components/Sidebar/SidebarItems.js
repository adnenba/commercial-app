import React from 'react';
import _ from 'lodash';
class SideBarItems extends React.PureComponent {
    constructor(props){
        super(props);
        this.state = {
            open: false,
            active: 0
        }
    }
    showListItems = () => {
        this.setState({open: !this.state.open});
        this.setState({active: !this.state.active})
    }
    render() {
        const { soulist, li } = this.props
        return (<li className={this.state.active ? "active" : ""}  >
        <div onClick={this.showListItems}  className="list-item" >{li}
            {!_.isEmpty(soulist) &&
                <span className="caret"></span>}</div>
            {this.state.open && !_.isEmpty(soulist) && 
             <ul className="nav nav-sidebar soulist">
                {soulist.map((newList, i) => {
                    return <li key={i}>{newList}</li>
                })}
             
                </ul>
            }
        </li>)
    }
}


export default SideBarItems;