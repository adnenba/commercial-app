import React from 'react';
import Aux from '../../hoc/Auxx';
import _ from  'lodash';
import SidebarItems from './SidebarItems';
const navSidebar = (props) => {
    const listStyle = props.listed.map(multiList => {
        return <SidebarItems key={multiList.li} {...multiList} />
    })
    return (
    <Aux>
        <ul className="nav nav-sidebar">
            {listStyle}
        </ul>
    </Aux>
)
}

export default navSidebar;