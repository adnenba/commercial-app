import React, {Component} from 'react';
import NavSidebar from './NavSidebar';
import './style.css';

// start  sidebar
class sideBar extends Component {
    state = {
    list:[
        {li:"Overview"},
        {li:"Reports"},
        {li:"Analytics", 
        soulist: ["Shop","Cart","Checkout", "Cards"]},
        {li:"Export", soulist: ["Shop","Cart","Checkout", "Cards"]},
        {li:"Nav item"},
        {li:"Nav item again"},
        {li:"One more nav"},
        {li:"Another nav item"},
        {li:"More navigation"}
    ]
}
     
    render(){
        return (
    <div className="col-sm-3 col-md-2 sidebar">
        <NavSidebar listed={this.state.list}/>
    </div>
);
    }
}


export default sideBar;