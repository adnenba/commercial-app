import React, { Component } from 'react';
import Sidebar from './components/Sidebar/Sidebar';
import Header from './components/Header/Header';
import Content from './containers/Content/Content';
import Layout from './Layout/Layout';
class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <div className="container">
            <div className="row">
        <Sidebar/>
        <Content />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
