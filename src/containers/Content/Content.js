import React, {Component} from 'react';
import Table from '../../components/Table/Table';

class Content extends Component{
    render(){
        const entites = [
            {
                nom: 'aaa',
                prenom: 'zzz',
                age: 12
            },
            {
                nom: 'ee',
                prenom: 'zff',
                age: 123
            },
            {
                nom: 'ff',
                prenom: 'ggg',
                age: 88
            }
        ];

        return (
        <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
        <p>Hello World</p>
        <Table entites={entites} />
        </div>
        )
    }
}

export default Content;